# Voltage Regulator

The voltage regulator takes power supplied by a 24V battery. It has two parts. One has a Zener diode that keeps a reference voltage of 4.7V. The second party consist of an NPN transistor the amplifies the voltage it receives to the desired range.